package mongoTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoTestMain {
	public static void main(String[] args) {
		SpringApplication.run(MongoTestMain.class, args);
	}
}
