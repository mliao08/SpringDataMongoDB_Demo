package mongoTest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value="/api/v1")
public class MongoTestRestController {
	@Autowired
	private UserMongoDao userMongoDao;
	@Autowired
	private TaskResultsDao taskResultsDao;
	
	/*The create operation*/
	@RequestMapping(value="/insert-data",method=RequestMethod.POST)
	public String InsertData(@RequestBody TaskResults taskResults) {
//		userMongoDao.insert(taskConfig);
		TaskResults results = taskResultsDao.findByTaskName(taskResults.getTaskName());
		if( null != results ){
			results.getResults().addAll(taskResults.getResults());
			taskResultsDao.save(results);
		}
		return "Success";
	}
	
	@RequestMapping(value="/save-data",method=RequestMethod.POST)
	public String SaveData(@RequestBody TaskResults taskResults) {
//		userMongoDao.save(taskConfig);
		taskResultsDao.save(taskResults);
		return "Success";
	}
	
	/*The read operation*/
	@RequestMapping(value="/query-data/{taskName}",method=RequestMethod.GET,produces="application/json")
	public TaskConfig QueryData(@PathVariable String taskName) {
		TaskConfig taskConfig = null;
		//taskConfig = userMongoRepository.findOne(taskName);
		taskConfig = userMongoDao.findByTaskName(taskName);
		
		return taskConfig;
	}
	
	/*The update opeartion*/
	@Deprecated
	@RequestMapping(value="/update-data/{taskName}/{fieldname}/{fieldvalue}",method=RequestMethod.POST,produces="application/json")
	public TaskConfig UpdateData(@PathVariable String taskName, @PathVariable String fieldname, @PathVariable String fieldvalue) {
		TaskConfig taskConfig = null;
		taskConfig = userMongoDao.findOne(taskName);
		if( null != taskConfig ){
			taskConfig.setTaskStatus(true);
		}
		
		return taskConfig;
	}
	
	@RequestMapping(value="/update-data/{taskName}",method=RequestMethod.POST,produces="application/json")
	public TaskConfig UpdateDataAll(@PathVariable String taskName, @RequestBody TaskConfig taskConfig) {
		userMongoDao.delete(taskName);
		userMongoDao.save(taskConfig);
		
		return taskConfig;
	}
	
	@RequestMapping(value="/delete-data/{taskName}",method=RequestMethod.DELETE)
	public String DeleteData(@PathVariable String taskName) {
		userMongoDao.delete(taskName);
		
		return "Success";
	}
}



/*
 * Request body(application-json,method=POST)
 * {
    "taskName": "FoshanWangba",
    "taskStatus": false,
    "ftpConfig": {
        "protocol": "ftp",
        "ftpServerIp": "172.30.6.81",
        "ftpServerPort": 0,
        "user": "liaoming",
        "password": "Barca1899",
        "dataSourceConfig": [
            {
                "remoteFilesDirectory": "/home/liaoming/testftp/nhwb",
                "primaryDirFilterPattern": "yyyyMMdd",
                "secondaryDirFilterPattern": "WA_BASIC_FJ_0001",
                "fileFilterRegex": ".*?(zip)",
                "localFilesDirectory": "/home/liaoming/tempFiles",
                "dataSyncTime": "now"
            },
            {
                "remoteFilesDirectory": "/home/liaoming/testftp/nhwb",
                "primaryDirFilterPattern": "yyyyMMdd",
                "secondaryDirFilterPattern": "WA_SOURCE_FJ_0001",
                "fileFilterRegex": ".*?(zip)",
                "localFilesDirectory": "/home/liaoming/tempFiles",
                "dataSyncTime": "now"
            }
        ]
    }
}*/


/*
 * 小结：
 * 1、定义一个类，与Mongo的Collection中要保存的文档结构对应，并用@Document(collection="xxx") 标准
 * 2、声明一个 MongoRepository接口，并用@Autowired标准，springboot会自动注入一个对象
 * 3、使用MongoRepository 接口中的方法操作Mongo的Collection*/

