package mongoTest;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserMongoDao extends MongoRepository<TaskConfig,String>{
	@Query(value="{'taskName':?0}"/*,fileds="{'id':1,'name':1}"*/)
	public TaskConfig findByTaskName(String taskName);
	
	/*
	 * @Query(value = "{'hostName':?0,'type':?1,'dbName':?2}")
    List<DataSource> getDataSource(String ip, String dbType, String dbName);
    */
}
