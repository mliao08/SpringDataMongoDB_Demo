package mongoTest;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskResultsDao extends MongoRepository<TaskResults,String>{
	TaskResults findByTaskName(String taskName);
}
