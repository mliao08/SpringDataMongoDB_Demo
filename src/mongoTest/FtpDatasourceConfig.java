package mongoTest;

public class FtpDatasourceConfig {
	private String remoteFilesDirectory;
	/*to help finding the remote working directory,
	if it's null then remoteFilesDirectory will be considered to be the working directory.*/
	private String primaryDirFilterPattern;  /*Use pattern,for example:yyyyMMdd to generate a date string to match the directory.*/
	private String secondaryDirFilterPattern;
	//private String directoryFilterRegex;  /*use regex to filter directory*/
	private String fileFilterRegex; /*to initiate the filefilter,use to filter files.*/
	private String localFilesDirectory;  /*temporarily use,decompress zip files to this directory.*/
	private String dataSyncTime;   /*yyyy-MM-dd HH:mm:ss/'now',interval is considered to be 24 hours.*/
	
	public String getRemoteFilesDirectory() {
		return remoteFilesDirectory;
	}
	public void setRemoteFilesDirectory(String remoteFilesDirectory) {
		this.remoteFilesDirectory = remoteFilesDirectory;
	}
	public String getPrimaryDirFilterPattern() {
		return primaryDirFilterPattern;
	}
	public void setPrimaryDirFilterPattern(String primaryDirFilterPattern) {
		this.primaryDirFilterPattern = primaryDirFilterPattern;
	}
	public String getSecondaryDirFilterPattern() {
		return secondaryDirFilterPattern;
	}
	public void setSecondaryDirFilterPattern(String secondaryDirFilterPattern) {
		this.secondaryDirFilterPattern = secondaryDirFilterPattern;
	}
	public String getFileFilterRegex() {
		return fileFilterRegex;
	}
	public void setFileFilterRegex(String fileFilterRegex) {
		this.fileFilterRegex = fileFilterRegex;
	}
	public String getLocalFilesDirectory() {
		return localFilesDirectory;
	}
	public void setLocalFilesDirectory(String localFilesDirectory) {
		this.localFilesDirectory = localFilesDirectory;
	}
	public String getDataSyncTime() {
		return dataSyncTime;
	}
	public void setDataSyncTime(String dataSyncTime) {
		this.dataSyncTime = dataSyncTime;
	}
	

	
}
