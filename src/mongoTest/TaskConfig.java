package mongoTest;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/*First we need to write a Mongo data class whose instance 
 * maps to a document stored in a mongo collection.*/
@Document(collection = "RemoteFileMngTask")
public class TaskConfig {
	@Id
	private String taskName;
	private Boolean taskStatus;
	private FtpConfig ftpConfig;
	
	
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Boolean getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(Boolean taskStatus) {
		this.taskStatus = taskStatus;
	}
	public FtpConfig getFtpConfig() {
		return ftpConfig;
	}
	public void setFtpConfig(FtpConfig ftpConfig) {
		this.ftpConfig = ftpConfig;
	}
	
	


}
